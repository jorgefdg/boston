const express = require('express')
const path = require('path')
const formidable = require('express-formidable');
var app = express()
const port = 3000

app.use(formidable());

app.use('/static', express.static(path.join(__dirname, 'public')))
app.get('/', (req, res) => res.sendFile(path.join(__dirname+'/index.html')))
app.post('/predict', (req, res) => {
	var spawn = require("child_process").spawn;
	var py_process = spawn('python',["./predict.py",
                            req.fields.rooms,
                            req.fields.poverty,
                            req.fields.teachers ] );

	py_process.stdout.on('data', function(data) {
        res.json({predicted_value:data.toString()});
    } )
	py_process.stderr.on('data', function(data) {
		console.error(data.toString());
        res.send(data.toString());
    } )
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))