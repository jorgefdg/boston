function submitHandler(event){;
	event.preventDefault()
	let submit_button = event.target.querySelector('button');
	let form_data = new FormData(event.target);
	submit_button.classList.add('disabled');
	fetch("/predict", {
		method: "POST",
		body: form_data
	})
	.then(function(r){
		r.json().then(function(value){
			if(value.predicted_value === undefined){
				window.M.toast({html:'Ha ocurrido un error'})
				return;
			}
			document.querySelector('#prediction').value = `Valor previsto: $${value.predicted_value}`;
			submit_button.classList.remove('disabled');
			event.target.reset();
			window.M.updateTextFields();
		})
	})
	.catch(function(error){
		console.log(error)
		submit_button.classList.remove('disabled');
	})
}

document.addEventListener("DOMContentLoaded", function() {
	let form = document.querySelector('form');
	form.addEventListener('submit', submitHandler);
});